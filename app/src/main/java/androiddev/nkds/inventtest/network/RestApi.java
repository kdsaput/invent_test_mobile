package androiddev.nkds.inventtest.network;

import androiddev.nkds.inventtest.model.PriceResponse;
import androiddev.nkds.inventtest.model.ProductResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RestApi {

    @GET("get_m_product")
    Call<ProductResponse> getProductAPI();

    @GET("get_product_price")
    Call<PriceResponse> getPriceAPI();
}
