package androiddev.nkds.inventtest.view.splash;

import android.app.Application;

import java.util.List;

import androiddev.nkds.inventtest.model.HasilModel;
import androiddev.nkds.inventtest.model.PriceModel;
import androiddev.nkds.inventtest.model.PriceResponse;
import androiddev.nkds.inventtest.model.ProductModel;
import androiddev.nkds.inventtest.model.ProductResponse;
import androiddev.nkds.inventtest.repository.GeneralRepository;
import androiddev.nkds.inventtest.utils.constant.S;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SplashViewModel extends AndroidViewModel {

    private GeneralRepository generalRepository;
    private LiveData<PriceResponse> priceResponse;
    private LiveData<ProductResponse> productResponse;
    private LiveData<List<PriceModel>> priceModelLiveData;
    private LiveData<List<ProductModel>> productModelLiveData;
    private boolean isDoneHit = false, isDoneHitProd = false;

    public SplashViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        generalRepository = new GeneralRepository();
        priceModelLiveData = generalRepository.getPriceLive();
        productModelLiveData = generalRepository.getProductLive();
    }


    public void hitAPiPrice(){
        priceResponse = generalRepository.hitPriceAPI();
    }

    public void hitAPiProduct(){
        productResponse = generalRepository.hitProductAPI();
    }

    public LiveData<PriceResponse> getPriceResponse() {
        return priceResponse;
    }

    public LiveData<ProductResponse> getProductResponse() {
        return productResponse;
    }

    public LiveData<List<PriceModel>> getPriceModelLiveData() {
        return priceModelLiveData;
    }

    public LiveData<List<ProductModel>> getProductModelLiveData() {
        return productModelLiveData;
    }

    public boolean isDoneHit() {
        return isDoneHit;
    }

    public void setDoneHit(boolean doneHit) {
        isDoneHit = doneHit;
    }

    public boolean isDoneHitProd() {
        return isDoneHitProd;
    }

    public void setDoneHitProd(boolean doneHitProd) {
        isDoneHitProd = doneHitProd;
    }
}
