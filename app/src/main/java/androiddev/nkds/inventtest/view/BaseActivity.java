package androiddev.nkds.inventtest.view;

import android.Manifest;
import android.os.Bundle;
import android.os.PersistableBundle;

import java.text.SimpleDateFormat;
import java.util.Locale;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity <B extends ViewDataBinding> extends AppCompatActivity {

    protected B bind;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bindView();
        initView();
        assignListener();
    }

    /*
     *   set data binding
     */
    private void bindView() {
        bind = DataBindingUtil.setContentView(this, contentView());
    }


    /*
     *   set layout xml
     */
    @LayoutRes
    public abstract int contentView();


    /*
     *   inisialisasi semua variable atau class
     */
    public abstract void initView();


    /*
     *   semua function untuk action seperti onclick dll
     */
    public abstract void assignListener();
}
