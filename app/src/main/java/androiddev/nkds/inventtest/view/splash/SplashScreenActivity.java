package androiddev.nkds.inventtest.view.splash;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import java.util.List;

import androiddev.nkds.inventtest.R;
import androiddev.nkds.inventtest.databinding.ActivitySplashBinding;
import androiddev.nkds.inventtest.model.PriceModel;
import androiddev.nkds.inventtest.model.PriceResponse;
import androiddev.nkds.inventtest.model.ProductModel;
import androiddev.nkds.inventtest.model.ProductResponse;
import androiddev.nkds.inventtest.utils.constant.S;
import androiddev.nkds.inventtest.view.BaseActivity;
import androiddev.nkds.inventtest.view.main.MainActivity;
import androiddev.nkds.inventtest.view.main.MainViewModel;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

public class SplashScreenActivity extends BaseActivity<ActivitySplashBinding> {

    private SplashViewModel viewModel;

    @Override
    public int contentView() {
        return R.layout.activity_splash;
    }

    @Override
    public void initView() {

        viewModel = ViewModelProvider.AndroidViewModelFactory.
                getInstance(this.getApplication()).create(SplashViewModel.class);
        new Handler().postDelayed(this::init, 1000);

    }

    @Override
    public void assignListener() {

    }

    private void init(){

        viewModel.init();
        subscribeValidate();
    }

    private void subscribeValidate(){

        viewModel.getPriceModelLiveData().observe(this, new Observer<List<PriceModel>>() {
            @Override
            public void onChanged(List<PriceModel> listResource) {

                if(listResource==null || listResource.size()==0){
                    if(!viewModel.isDoneHit()) {
                        viewModel.hitAPiPrice();
                        subscribeOberversPrice();
                    }
                }
                else {
                    if(!viewModel.isDoneHit()) {
                        subscribeProduct();
                    }
                }
            }
        });
    }

    private void subscribeProduct(){
        viewModel.getProductModelLiveData().observe(this, new Observer<List<ProductModel>>() {
            @Override
            public void onChanged(List<ProductModel> listResource) {
                if(listResource==null || listResource.size()==0){
                    if(!viewModel.isDoneHitProd()) {
                        viewModel.hitAPiProduct();
                        subscribeOberversProduct();
                    }
                }
                else {
                    if(!viewModel.isDoneHitProd()) {
                        startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                        finishAffinity();
                    }
                }
            }
        });
    }

    private void subscribeOberversPrice(){

        viewModel.getPriceResponse().observe(this, new Observer<PriceResponse>() {
            @Override
            public void onChanged(PriceResponse listResource) {

                Log.e("status",listResource.getStatusCode());
                    switch (listResource.getStatusCode()){

                        case S.LOADING_API_CODE:{
                            setVisibleLoading(true);
                            break;
                        }

                        case S.SUCCESS_API_CODE:{
                            viewModel.setDoneHit(true);
                            setVisibleLoading(false);
                            subscribeProduct();
                            break;
                        }

                        default:{
                            setVisibleLoading(false);
                            setVisibleError(true,listResource.getStatusMsg());

                            break;
                        }

                    }
            }
        });
    }

    private void subscribeOberversProduct(){

        viewModel.getProductResponse().observe(this, new Observer<ProductResponse>() {
            @Override
            public void onChanged(ProductResponse listResource) {
                if(listResource != null){
                    switch (listResource.getStatusCode()){

                        case S.LOADING_API_CODE:{
                            setVisibleError(false,"");
                            setVisibleLoading(true);
                            break;
                        }

                        case S.SUCCESS_API_CODE:{
                            viewModel.setDoneHitProd(true);
                            setVisibleLoading(false);
                            startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                            finishAffinity();
                            break;
                        }

                        default:{
                            setVisibleLoading(false);
                            setVisibleError(true,listResource.getStatusMsg());
                            bind.lyError.lyError.setVisibility(View.VISIBLE);

                            break;
                        }

                    }
                }
            }
        });
    }

    private void setVisibleLoading(boolean isVisible){

        if(isVisible){
            bind.lyLoading.lyLoading.setVisibility(View.VISIBLE);
        }
        else {
            bind.lyLoading.lyLoading.setVisibility(View.GONE);
        }
    }

    private void setVisibleError(boolean isVisible,String msg){

        if(isVisible){

            bind.tvSplash.setVisibility(View.GONE);
            bind.lyError.tvError.setText(msg);
            bind.lyError.lyError.setVisibility(View.VISIBLE);
        }
        else {
            bind.tvSplash.setVisibility(View.VISIBLE);
            bind.lyError.lyError.setVisibility(View.GONE);
        }
    }
}
