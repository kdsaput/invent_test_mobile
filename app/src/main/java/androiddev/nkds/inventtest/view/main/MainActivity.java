package androiddev.nkds.inventtest.view.main;

import android.util.Log;
import android.view.View;

import androiddev.nkds.inventtest.R;
import androiddev.nkds.inventtest.adapter.HasilAdapter;
import androiddev.nkds.inventtest.adapter.ListCustomAdapter;
import androiddev.nkds.inventtest.databinding.ActivityMainBinding;
import androiddev.nkds.inventtest.model.HasilModel;
import androiddev.nkds.inventtest.model.ParamModel;
import androiddev.nkds.inventtest.view.BaseActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import static android.view.View.GONE;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    private MainViewModel viewModel;
    private ListCustomAdapter listCustomAdapter;
    private HasilAdapter adapter;

    @Override
    public int contentView() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {

        bind.toolbar.tvToolbarName.setText("List");
        adapter = new HasilAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        bind.rvData.setLayoutManager(linearLayoutManager);
        bind.rvData.setHasFixedSize(true);
        bind.rvData.setAdapter(adapter);
        viewModel = ViewModelProvider.AndroidViewModelFactory.
                getInstance(this.getApplication()).create(MainViewModel.class);
        viewModel.init();
        subscribeParam();
        subscribeObervers();
    }

    @Override
    public void assignListener() {

        bind.ivSearch.setOnClickListener(v -> {

            viewModel.setSearching(bind.etSearch.getText().toString().trim());
        });

        bind.toolbar.ryToolbarFilter.setOnClickListener(v -> {

            showList();
        });

        bind.ivSearchPrice.setOnClickListener(v->{
            viewModel.setSearchingPrice(bind.etSearchPrice.getText().toString().trim());
        });

    }

    private void subscribeParam(){
        viewModel.getParamModelMutableLiveData().observe(this, new Observer<ParamModel>() {
            @Override
            public void onChanged(ParamModel paramModel) {
                Log.e("param","changed");
                bind.lyLoading.lyLoading.setVisibility(View.VISIBLE);
                viewModel.hitApi();
                subscribeObervers();
            }
        });
    }

    private void subscribeObervers() {

        viewModel.getHasil().observe(this, new Observer<List<HasilModel>>() {
            @Override
            public void onChanged(List<HasilModel> hasilModels) {

                Log.e("masuk","changed");
                if (hasilModels.size() > 0) {
                    adapter.setListHasil(hasilModels);
                    bind.lyError.lyError.setVisibility(View.GONE);
                    bind.rvData.setVisibility(View.VISIBLE);
                } else {
                    bind.lyError.tvError.setText("Data not found");
                    bind.lyError.lyError.setVisibility(View.VISIBLE);
                    bind.rvData.setVisibility(View.GONE);
                }
                bind.lyLoading.lyLoading.setVisibility(GONE);
            }
        });
    }

    private void showList() {

        listCustomAdapter = new ListCustomAdapter(viewModel.getFilter());
        listCustomAdapter.setOnItemClickListener(new ListCustomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                viewModel.setSort(position);
               // subscribeObervers();
                bind.lyDialogCustomeList.lyDialogLayout.setVisibility(GONE);
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        bind.lyDialogCustomeList.rvCustomList.setLayoutManager(mLayoutManager);
        bind.lyDialogCustomeList.rvCustomList.setItemAnimator(new DefaultItemAnimator());
        bind.lyDialogCustomeList.rvCustomList.setAdapter(listCustomAdapter);
        bind.lyDialogCustomeList.lyDialogLayout.setVisibility(View.VISIBLE);
        if (viewModel.getPos() != -1) {
            listCustomAdapter.selectedPosition = viewModel.getPos();
            listCustomAdapter.notifyDataSetChanged();
        }
    }
}