package androiddev.nkds.inventtest.view.main;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androiddev.nkds.inventtest.model.HasilModel;
import androiddev.nkds.inventtest.model.ParamModel;
import androiddev.nkds.inventtest.repository.GeneralRepository;
import androiddev.nkds.inventtest.utils.constant.S;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class MainViewModel extends AndroidViewModel {

    private GeneralRepository generalRepository;
    private LiveData<List<HasilModel>> hasil;
    private List<String> filter = new ArrayList<>();
    private ParamModel paramModel = new ParamModel();
    private MutableLiveData<ParamModel> paramModelMutableLiveData;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        generalRepository = new GeneralRepository();
        paramModelMutableLiveData = new MutableLiveData<>();
        hitApi();
        filter.add(S.NAMA_BARANG);
        filter.add(S.HARGA_BARANG);
    }

    public LiveData<List<HasilModel>> getHasil() {
        return hasil;
    }

    public void setSearching(String searching){

        if(!TextUtils.isEmpty(searching)) {
            paramModel.setSearch("%"+searching+"%");

        }
        else {
            paramModel.setSearch("%%");

        }
        paramModelMutableLiveData.postValue(paramModel);
    }

    public void setSearchingPrice(String searching){
        int price = 0;
        if(!TextUtils.isEmpty(searching)) {
            if(TextUtils.isDigitsOnly(searching)) {
                try {
                    price = Integer.parseInt(searching);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        paramModel.setPrice(price);
        paramModelMutableLiveData.postValue(paramModel);
    }

    public void setSort(int pos){

        paramModel.setPos(pos);
        paramModelMutableLiveData.postValue(paramModel);
    }

    public int getPos(){
        return paramModel.getPos();
    }

    public List<String> getFilter() {
        return filter;
    }

    public MutableLiveData<ParamModel> getParamModelMutableLiveData() {
        return paramModelMutableLiveData;
    }

    public void hitApi(){
        hasil = generalRepository.getDataGeneral(paramModel);
    }
}
