package androiddev.nkds.inventtest.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import androiddev.nkds.inventtest.R;
import androiddev.nkds.inventtest.model.HasilModel;
import androiddev.nkds.inventtest.utils.ImageHelper;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HasilAdapter extends RecyclerView.Adapter<HasilAdapter.ViewHolder> {

    private List<HasilModel> listHasil;
    private Context mContext;

    public HasilAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public HasilAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycle, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HasilAdapter.ViewHolder holder, int position) {

            HasilModel childText = listHasil.get(position);

            holder.tvName.setText(childText.getNamaBarang());
            holder.tvPrice.setText(childText.getPrice()+"");
            holder.tvCabang.setText(childText.getCabang());

            ImageHelper.setImageToImageView(mContext, holder.ivFoto, childText.getUrlImage());
    }

    @Override
    public int getItemCount() {

        if (listHasil != null && listHasil.size() > 0) {
            return listHasil.size();
        }

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvPrice, tvCabang;
        ImageView ivFoto;
        RelativeLayout rvDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = (TextView) itemView
                    .findViewById(R.id.tv_name);
            tvPrice = (TextView) itemView
                    .findViewById(R.id.tv_price);
            tvCabang = (TextView) itemView
                    .findViewById(R.id.tv_cabang);
            ivFoto = (ImageView) itemView
                    .findViewById(R.id.iv_image);

        }
    }


    public List<HasilModel> getListHasil() {
        return listHasil;
    }

    public void setListHasil(List<HasilModel> listHasil) {
        this.listHasil = listHasil;
        notifyDataSetChanged();
    }

}

