package androiddev.nkds.inventtest.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androiddev.nkds.inventtest.R;
import androiddev.nkds.inventtest.databinding.ItemListCustomeBinding;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ListCustomAdapter extends RecyclerView.Adapter<ListCustomAdapter.ViewHolder> {

    private ItemListCustomeBinding mBinding;
    private List<String> stringList;
    public int selectedPosition = -1;
    private OnItemClickListener mItemClickListener;


    public ListCustomAdapter(List<String> stringList) {
        this.stringList = stringList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_list_custome, parent, false);
        return new ViewHolder(mBinding.getRoot(), mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String itemShow = "";

        itemShow = stringList.get(position);
        holder.mBinding.tvItem.setText(itemShow);

        if (selectedPosition != -1 && selectedPosition == position) {
            holder.mBinding.itemList.setSelected(true);
            holder.mBinding.itemList.setBackgroundColor(holder.view.getResources().getColor(R.color.textColorSelected));
        } else {
            holder.mBinding.itemList.setSelected(false);
            holder.mBinding.itemList.setBackgroundColor(holder.view.getResources().getColor(android.R.color.transparent));
        }

        holder.mBinding.itemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPosition >= 0)
                    notifyItemChanged(selectedPosition);
                selectedPosition = holder.getAdapterPosition();
                notifyItemChanged(selectedPosition);

                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);
                }
                selectedPosition = holder.getAdapterPosition();
            }
        });
    }

    @Override
    public int getItemCount() {

        if (stringList != null && stringList.size() > 0) {
            return stringList.size();
        } else {
            return 0;
        }
    }

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemListCustomeBinding mBinding;
        View view;

        public ViewHolder(View itemView, ItemListCustomeBinding mBinding) {
            super(itemView);
            view = itemView;
            this.mBinding = mBinding;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}

