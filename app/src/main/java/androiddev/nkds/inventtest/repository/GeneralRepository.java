package androiddev.nkds.inventtest.repository;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import androiddev.nkds.inventtest.MainApplication;
import androiddev.nkds.inventtest.model.GeneralDao;
import androiddev.nkds.inventtest.model.HasilModel;
import androiddev.nkds.inventtest.model.ParamModel;
import androiddev.nkds.inventtest.model.PriceModel;
import androiddev.nkds.inventtest.model.PriceResponse;
import androiddev.nkds.inventtest.model.ProductModel;
import androiddev.nkds.inventtest.model.ProductResponse;
import androiddev.nkds.inventtest.network.RestApi;
import androiddev.nkds.inventtest.network.RestClient;
import androiddev.nkds.inventtest.utils.InternetConnectionHelper;
import androiddev.nkds.inventtest.utils.constant.S;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralRepository {

    private GeneralDao generalDao;
    private RestApi restApi;
    private MutableLiveData<PriceResponse> priceModel;
    private MutableLiveData<ProductResponse> productModel;
    private LiveData<List<PriceModel>> priceLive;
    private LiveData<List<ProductModel>> productLive;

    public GeneralRepository() {
        restApi = RestClient.getInstance().getApiService();
        generalDao = MainApplication.getDatabase().generalDao();
        priceModel = new MutableLiveData<>();
        productModel = new MutableLiveData<>();
        productLive = generalDao.checkExistProduct();
        priceLive = generalDao.checkExistPrice();


    }


    public LiveData<List<HasilModel>> getDataGeneral(ParamModel paramModel) {

        if (paramModel.getPos() == 0) {
            return generalDao.getProductWithFilterSortNama(paramModel.getSearch(), paramModel.getPrice());
        } else {
            return generalDao.getProductWithFilterSortHarga(paramModel.getSearch(), paramModel.getPrice());
        }

    }

    public void insertPriceRoom(List<PriceModel> models) {
        new InsertPriceAsyncTask(generalDao).execute(models);
    }

    private static class InsertPriceAsyncTask extends AsyncTask<List<PriceModel>, Void, Void> {

        private GeneralDao generalDao;

        private InsertPriceAsyncTask(GeneralDao generalDao) {
            this.generalDao = generalDao;
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(List<PriceModel>... lists) {
            generalDao.insertPrice(lists[0]);
            return null;
        }
    }

    public void insertProductRoom(List<ProductModel> models) {
        new InsertProductAsyncTask(generalDao).execute(models);
    }

    private static class InsertProductAsyncTask extends AsyncTask<List<ProductModel>, Void, Void> {

        private GeneralDao generalDao;

        private InsertProductAsyncTask(GeneralDao generalDao) {
            this.generalDao = generalDao;
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(List<ProductModel>... lists) {
            generalDao.insertProduct(lists[0]);
            return null;
        }
    }

    public LiveData<PriceResponse> hitPriceAPI() {

        PriceResponse priceResponse = new PriceResponse();
        priceResponse.setStatusCode(S.LOADING_API_CODE);
        priceModel.postValue(priceResponse);
        Log.e("masuk", "api");
        if (InternetConnectionHelper.checkConnection()) {
            restApi.getPriceAPI()
                    .enqueue(new Callback<PriceResponse>() {
                        @Override
                        public void onResponse(Call<PriceResponse> call, Response<PriceResponse> response) {
                            Log.e("json", new Gson().toJson(response.body()));
                            if (response.body() != null && response.body().getStatusCode() != null) {

                                insertPriceRoom(response.body().getPriceModelList());

                                PriceResponse priceResponse = new PriceResponse();
                                priceResponse.setStatusCode(response.body().getStatusCode());
                                priceResponse.setStatusMsg(response.body().getStatusMsg());
                                if (response.body().getPriceModelList() != null) {
                                    priceResponse.setPriceModelList(response.body().getPriceModelList());
                                }
                                Log.e("response", new Gson().toJson(priceResponse));
                                priceModel.postValue(priceResponse);


                            } else {
                                Log.e("masuk sini", "gagal");
                                PriceResponse priceResponse = new PriceResponse();
                                priceResponse.setStatusCode(S.ERR_API_CODE_GEN);
                                priceResponse.setStatusMsg("Something went wrong, try again");
                                priceModel.postValue(priceResponse);
                            }
                        }

                        @Override
                        public void onFailure(Call<PriceResponse> call, Throwable t) {

                            PriceResponse priceResponse = new PriceResponse();
                            priceResponse.setStatusCode(S.ERR_API_CODE_GEN);
                            priceResponse.setStatusMsg("Something went wrong, try again");
                            priceModel.postValue(priceResponse);
                        }

                    });
        } else {
            priceResponse = new PriceResponse();
            priceResponse.setStatusCode(S.ERR_API_CODE_GEN);
            priceResponse.setStatusMsg("Check your internet connection");
            priceModel.postValue(priceResponse);
        }


        return priceModel;
    }

    public LiveData<ProductResponse> hitProductAPI() {
        ProductResponse responese = new ProductResponse();
        responese.setStatusCode(S.LOADING_API_CODE);
        productModel.postValue(responese);

        if (InternetConnectionHelper.checkConnection()) {
            restApi.getProductAPI()
                    .enqueue(new Callback<ProductResponse>() {
                        @Override
                        public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                            if (response.body() != null && response.body().getStatusCode() != null) {

                                insertProductRoom(response.body().getProductModelList());
                                productModel.postValue(response.body());


                            } else {

                                ProductResponse responese = new ProductResponse();
                                responese.setStatusCode(S.LOADING_API_CODE);
                                responese.setStatusMsg("Something went wrong, try again");
                                productModel.postValue(responese);
                            }
                        }

                        @Override
                        public void onFailure(Call<ProductResponse> call, Throwable t) {
                            ProductResponse responese = new ProductResponse();
                            responese.setStatusCode(S.LOADING_API_CODE);
                            responese.setStatusMsg("Something went wrong, try again");
                            productModel.postValue(responese);
                        }

                    });
        } else {
            responese = new ProductResponse();
            responese.setStatusCode(S.ERR_API_CODE_GEN);
            responese.setStatusMsg("Check your internet connection");
            productModel.postValue(responese);
        }

        return productModel;
    }

    public MutableLiveData<PriceResponse> getPriceModel() {
        return priceModel;
    }

    public MutableLiveData<ProductResponse> getProductModel() {
        return productModel;
    }

    public LiveData<List<PriceModel>> getPriceLive() {
        return priceLive;
    }

    public LiveData<List<ProductModel>> getProductLive() {
        return productLive;
    }
}
