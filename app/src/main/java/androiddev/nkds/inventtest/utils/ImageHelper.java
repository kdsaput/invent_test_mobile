package androiddev.nkds.inventtest.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import androiddev.nkds.inventtest.R;

public class ImageHelper {

    public static void setImageToImageView(Context context, ImageView imageView, String imageUrl) {
        RequestOptions options = new RequestOptions()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);

        Glide.with(context)
                .load(imageUrl)
                .placeholder(R.color.placeholder)
                .apply(options)
                .into(imageView);
    }
}
