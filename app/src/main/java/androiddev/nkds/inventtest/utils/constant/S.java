package androiddev.nkds.inventtest.utils.constant;

public class S {

    public final static String NAMA_BARANG = "Nama Barang";
    public final static String HARGA_BARANG = "Price";
    public final static String BASE_URL = "https://invent-integrasi.com/test_core/v1/";
    public final static String SUCCESS_API_CODE = "ERR-00-000";
    public final static String ERR_API_CODE_GEN = "ERR-99-999";
    public final static String LOADING_API_CODE = "LOADING";
}
