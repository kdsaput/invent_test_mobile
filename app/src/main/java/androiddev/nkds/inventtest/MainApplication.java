package androiddev.nkds.inventtest;

import android.app.Application;
import android.content.Context;

import androiddev.nkds.inventtest.model.DatabaseInit;
import androiddev.nkds.inventtest.network.RestClient;

public class MainApplication extends Application {

    public static DatabaseInit database;
    public static Context globalContext;

    @Override
    public void onCreate() {
        super.onCreate();

        //NETWORK
        RestClient.start(this);

        //GLOBAL CONTEXT
        globalContext = getApplicationContext();

        database = DatabaseInit.getInstance();
    }

    public static DatabaseInit getDatabase() {
        return MainApplication.database;
    }
}
