package androiddev.nkds.inventtest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import androiddev.nkds.inventtest.model.PriceModel;

public class PriceResponse {

    @SerializedName("status_code")
    private String statusCode;

    @SerializedName("status_message_eng")
    private String statusMsg;

    @SerializedName("value")
    private List<PriceModel> priceModelList;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public List<PriceModel> getPriceModelList() {
        return priceModelList;
    }

    public void setPriceModelList(List<PriceModel> priceModelList) {
        this.priceModelList = priceModelList;
    }
}
