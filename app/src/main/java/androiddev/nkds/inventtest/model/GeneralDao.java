package androiddev.nkds.inventtest.model;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface GeneralDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertProduct(List<ProductModel> model);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertPrice(List<PriceModel> model);

    @Query("SELECT * FROM price_table")
    LiveData<List<PriceModel>> checkExistPrice();

    @Query("SELECT * FROM product_table")
    LiveData<List<ProductModel>> checkExistProduct();

    @Query("SELECT price_table.namaBarang, price_table.price, price_table.cabang," +
            " product_table.urlImage FROM price_table JOIN product_table ON price_table.kodeBarang = product_table.kodeBarang " +
            "WHERE price_table.namaBarang LIKE :where AND price >= :price ORDER BY price_table.namaBarang ASC")
    LiveData<List<HasilModel>> getProductWithFilterSortNama(String where, int price);

    @Query("SELECT price_table.namaBarang, price_table.price, price_table.cabang," +
            " product_table.urlImage FROM price_table JOIN product_table ON price_table.kodeBarang = product_table.kodeBarang " +
            "WHERE price_table.namaBarang LIKE :where AND price >= :price ORDER BY price ASC")
    LiveData<List<HasilModel>> getProductWithFilterSortHarga(String where, int price);



}
