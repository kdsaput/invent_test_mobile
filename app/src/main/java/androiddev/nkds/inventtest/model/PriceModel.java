package androiddev.nkds.inventtest.model;

import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "price_table")
public class PriceModel {

    @NonNull
    @PrimaryKey
    @SerializedName("id")
    private String id;

    @SerializedName("kode_barang")
    private String kodeBarang;

    @SerializedName("nama_barang")
    private String namaBarang;

    @SerializedName("harga_barang")
    private float price;

    @SerializedName("cabang")
    private String cabang;

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }
}
