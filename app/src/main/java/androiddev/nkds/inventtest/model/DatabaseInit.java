package androiddev.nkds.inventtest.model;

import androiddev.nkds.inventtest.MainApplication;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {PriceModel.class, ProductModel.class}, version = 1, exportSchema = false)
public abstract class DatabaseInit extends RoomDatabase {

    private static DatabaseInit instance;

    public abstract GeneralDao generalDao();

    public static synchronized DatabaseInit getInstance() {

        if (instance == null) {
            instance = Room.databaseBuilder(MainApplication.globalContext,
                    DatabaseInit.class, "invent_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }
}