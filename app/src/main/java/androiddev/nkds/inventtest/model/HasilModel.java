package androiddev.nkds.inventtest.model;

import com.google.gson.annotations.SerializedName;

import androidx.room.Embedded;

public class HasilModel {

    @SerializedName("nama_barang")
    private String namaBarang;

    @SerializedName("harga_barang")
    private float price;

    @SerializedName("cabang")
    private String cabang;

    @SerializedName("image")
    private String urlImage;

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
